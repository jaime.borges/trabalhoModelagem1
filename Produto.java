
package trabalhomodelagemsistemas;

public class Produto {
    //Atributos
    private String descricao;
    private int    preco;
    
    //Métodos
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }
    
}
