
package trabalhomodelagemsistemas;

public class ItemCompra {
    //Atributos
    private int   quantidade;
    private float valor;
    
    //Métodos
    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "ItemCompra{" + "quantidade=" + quantidade + ", valor=" + valor + '}';
    }
    
    
    
}
