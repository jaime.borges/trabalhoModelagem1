
package trabalhomodelagemsistemas;



public class Pessoa {
    //Atributos
    private String nome;
    private String sobrenome;

    
    //Métodos 
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "nome=" + nome + ", sobrenome=" + sobrenome + '}';
    }
    
    
}
    



