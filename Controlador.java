
package trabalhomodelagemsistemas;

public class Controlador {
       //Atributos
       private final  String clienteCorrente = "Pessoa";
       
       public static Controlador controlador = null;
       
       //Controladora
       
       public static Controlador getInstance() throws Exception{
           if(controlador == null){
               controlador = new Controlador ();
               return null;
           }else {
               throw new Exception ("Controlador já foi criado!");
           }         
       }
      
       //Métodos
        private Controlador(){
           
       }
       public void cadastrarCliente (String nome, String sobrenome){
           
       }
       public void cadastrarProduto(String descricao, float preco){
           
       }
       public void pesquisarComprasPorNome(String nome){

       }
       public void comprarProduto(String descricao){
           
       }
       public void listarProdutos(){
           
       }
       
      //Métodos Especiais

    public static Controlador getControlador() {
        return controlador;
    }

    public static void setControlador(Controlador controlador) {
        Controlador.controlador = controlador;
    }
       

}
   

