
package trabalhomodelagemsistemas;

public class Compra{
    //atributos
    private String numero;
    
    
    //Métodos
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Compra{" + "numero=" + numero + '}';
    }
    
    
}
